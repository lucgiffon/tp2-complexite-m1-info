import os
import random
import sys

def getKeyByValue(dict, value):
	""" Return the key of a given value

	@param dict (dict) The dictionnary where we look for the value
	@param value The looked for value
	@return key The key of the given value
	"""
	for key in dict:
		if dict[key] == value:
			return key

def genGraph(nbrVertex, nbrEdges):
	""" Return a graph like "3 2 1 2 2 3" (string)

	@param nbrVertex The number of vertex in the graph
	@param nbrEdges The number of edges in the graph
	@return graph (String) where:
	 -the first int is the number of vertex,
	 -the second int is the number of edges
	 -all following pairs are the vertex of each edges
	"""
	i = 0
	listEdges = []
	while i < nbrEdges:
		v1 = random.randint(1, nbrVertex)
		v2 = random.randint(1, nbrVertex)
		if ((v1, v2) not in listEdges) and ((v2, v1) not in listEdges) and (v1 != v2):
			listEdges.append((v1, v2))
		else:
			continue
		i += 1

	graph = str(nbrVertex) + " " + str(nbrEdges) + " "
	for edge in listEdges:
		graph += str(edge[0]) + " " + str(edge[1]) + " "

	return graph

def isInt(s):
	""" Return a Boolean which say if the given string is an int or not

	@param s (string)
	"""
	try:
		int(s)
		return True
	except ValueError:
		return False

def createVariables(nbrVertex, nbrCol):
	""" Return a dictionnary of 3SAT formula for Coloration variables associating strings to Dimacs variables

	@param nbrVertex (int) thje nbr of vertex in the graph
	@param nbrCol (int) the number of color allowed in the graph
	@return variables (dict)
	"""
	variables = {}
	numVariable = 1
	for vertex in range(1, nbrVertex + 1):
		for color in range(1, nbrCol + 1):
			variables[str(vertex) + str(color)] = numVariable
			numVariable += 1
	return variables

def createTuplesForEdges(splittedGraph):
	""" Retourne une liste de tuples correspondants aux sommets des arretes du graphe

	@param splittedGraph (list)
	@return listArretes (list)

	"""
	listArretes = []
	indexVertex = 2
	while indexVertex < len(splittedGraph):
		listArretes.append((splittedGraph[indexVertex], splittedGraph[indexVertex + 1]))
		indexVertex += 2

	return listArretes

def create3COLDimacsFormula(variables, listArretes, verbose = 0):
	""" Retourne une string contenant la formule CNF associée au graphe au format Dimacs

	@param variables (dict)
	@param listarretes(list)
	@param verbose (bool)
	@return to_write(string)
	"""
	to_write = ""

	to_write += "c formule phig_" + str(nbrVertex) + " 22\n"
	to_write += "c\n"

	to_write += "p cnf " + str(len(variables)) + " " + str(nbrEdges * 3 + nbrVertex) + "\n"

# --- Chaque sommet est colorié --- #

	for vertex in range(1, nbrVertex + 1):
		for color in range(1, nbrCol + 1):
			to_write += str(variables[str(vertex) + str(color)]) + " "
		to_write += "0\n"

# --- Deux sommets d'une arrête ne sont pas coloriés --- #

	for arete in listArretes:
		for color in range(1, nbrCol + 1):
			to_write += "-" + str(variables[str(arete[0]) + str(color)]) + " " + "-" + str(variables[str(arete[1]) + str(color)]) + " 0\n"

	if verbose == 1:
		print(to_write)

	return to_write

def getResults(fileName):
	""" Lit le fichier de résultats de minisat pour obtenir une liste de valuation. Si la formule est UNSAT, quitte.

	@param fileName (string)
	@return results (list)
	"""
	f = open(fileName, 'r')
	if (f.readline().strip() == "SAT"):
		print("Le graphe est 3 coloriable")
	else:
		exit("Le graphe n'est pas 3 coloriable")

	results = (f.readline())
	f.close()
	print(results)
	return results.split()

def writeFile(fileName, to_write):
	""" Ecris le fichier contenant la formule au format Dimacs

	@param fileName (string)
	@param to_write (string)
	"""
	f = open(fileName, 'w')
	f.write(to_write)
	f.close()

def getColors(splittedResults):
	""" Calcule les couleurs pour les sommets a partir des valuations

	@param splittedResults (list)
	"""
	for result in splittedResults:
		if result[0] == "-" or result[0] == "0":
			continue
		else:
			print("sommet: " + getKeyByValue(variables, int(result[0]))[0] +
				  "  ->  couleur: " + getKeyByValue(variables, int(result[0]))[1])

if __name__ == '__main__':

# --- Vérifie que l'input utilisateur est correct --- #
	if len(sys.argv) < 2 or len(sys.argv) > 4 :
		exit("usage: python3 phyg.py {nbrVertex} [nbrEdges|density]  ")

	if (isInt(sys.argv[1])):
		nbrVertex = int(sys.argv[1])
	else:
		exit("Le nombre de sommets doit être un entier")

	if len(sys.argv) == 3:
		if  (isInt(sys.argv[2])):
			nbrEdges = int(sys.argv[2])
		else:
			nbrEdges = int(((nbrVertex - 1) * nbrVertex / 2) * (float(sys.argv[2]) % 1))

	if len(sys.argv) == 2:
		nbrEdges = int(((nbrVertex - 1) * nbrVertex / 2))



	if nbrEdges > ((nbrVertex - 1) * nbrVertex / 2):
		exit("Il ne peut pas y avoir autant d'aretes (" + str(nbrEdges) + ") dans un graphe de " + str(nbrVertex) + " sommets.")



	nbrCol = 3
	graph = genGraph(nbrVertex, nbrEdges)

	splittedGraph = graph.split()
	print("Vérification de la 3 colorabilité du graphe G: " + graph + "\n" +
		"Nombre de sommet = " + str(nbrVertex) + "\n" +
		"Nombre d'arrêtes = " + str(nbrEdges) + "\n")
# --- Définition des variables --- #

	variables = createVariables(nbrVertex, nbrCol)

# --- Définition des arêtes --- #

	listArretes = createTuplesForEdges(splittedGraph)

# --- Ecriture au format Dimacs --- #

	to_write = create3COLDimacsFormula(variables, listArretes, 1)

	writeFile("input_phig_" + str(nbrVertex) + ".txt", to_write)

# --- Execution de minisat --- #

	os.system("minisat " + "input_phig_" + str(nbrVertex) + ".txt" + " " + "output_phig_" + str(nbrVertex) + ".txt")

# --- Lecture du fichier d'output --- #

	splittedResults = getResults("output_phig_" + str(nbrVertex) + ".txt")

	getColors(splittedResults)

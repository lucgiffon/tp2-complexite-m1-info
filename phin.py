import sys

def fact(n):
    """fact(n): calcule la factorielle de n (entier >= 0)"""
    if n < 2:
        return 1
    else:
        return n*fact(n-1)

if __name__ == '__main__':
	n = int(sys.argv[1])
	nbrChaussette = n + 1
	nbrTiroir = n

	variables = {}
	numVariable = 1
	for i in range(1, nbrChaussette + 1):
		for j in range(1, nbrTiroir + 1):
			variables[str(i) + str(j)] = numVariable
			numVariable += 1

	to_write = ""

	nbrClauses = 0

	clauses_to_write = ""
	# chaque chaussette doit être dans un tiroir
	for i in range(1, nbrChaussette + 1):
		for j in range(1, nbrTiroir + 1):
			clauses_to_write += (str(variables[str(i) + str(j)]) + " ")
		nbrClauses += 1
		clauses_to_write += "0\n"

	# chaque tiroir ne doit contenir qu'une seule chaussette
	for j in range(1, n + 1):
		for i in range(1, n + 1):
			for k in range(i + 1, n + 2):
				clauses_to_write += ("-" + str(variables[str(i) + str(j)]) + " " +
				      "-" + str(variables[str(k) + str(j)]) + " 0\n")
				nbrClauses += 1

	to_write += "c formule phi_" + str(n) + " e2\n"
	to_write += "c\n"
	to_write += "p cnf " + str(len(variables)) + " " + str(nbrClauses) + "\n"
	to_write += clauses_to_write
	print(to_write)



	f = open("input_phi" + str(n) + ".txt", 'w')
	f.write(to_write)
	f.close()
